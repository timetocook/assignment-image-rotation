#include "formats/image.h"

#ifndef ROTATE_H
#define ROTATE_H
struct image_status *rotate(struct image const* input_image);
#endif
