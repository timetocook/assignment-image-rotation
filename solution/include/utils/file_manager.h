#include <inttypes.h>

enum file_open_status  {
	OPEN_OK = 0,
	OPEN_FAILED
};
struct file_status{
	int code;
	char *message;
	FILE *file;
};
struct file_status *file_status_init(int code, char *message, FILE *file);
struct file_status *open_file(char const* path, char const* mode);
struct file_status *read_from_file(void *buffer, FILE *input_file);
struct file_status *write_to_file(void *buffer, FILE *output_file);
struct file_status *seek_of_file(uint32_t offset_bits, FILE *output_file);
void free_file_status(struct file_status *file);
