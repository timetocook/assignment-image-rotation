#include "utils/print_manager.h"
#include <stdio.h>
#include <stdlib.h>

void print_exception(int code, char *message) {
	if (code!=0) {
		fprintf(stderr, "%s\n", message);
		exit(code);
	} else {
		fprintf(stdout, "%s\n", message);
	}
}


